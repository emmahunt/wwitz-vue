<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Config;

class DropdownController extends BaseController	
{

    public function formatData() {
		$schema=config::get('database.connections.sqlsrv.schema');

		if($schema == 'STABLE'){
			$reference_schema = 'REFERENCE';
		} elseif($schema == 'DEVELOPMENT') {
			$reference_schema = 'DEVELOPMENT';
		} else {
			$reference_schema = '';
		}

		$hierarchy = DB::table($reference_schema.'.HIERARCHY')->where('Active','=',1)->get();
		$obj = [];
		$map = [];

		$locations=DB::table($reference_schema.'.HIERARCHY')->distinct()->where('Active','=',1)->pluck('Location_Name');
		$businessUnits=DB::table($reference_schema.'.HIERARCHY')->distinct()->where('Active','=',1)->pluck('Business_Unit');
		$serviceAreas=DB::table($reference_schema.'.HIERARCHY')->distinct()->where('Active','=',1)->pluck('Service_Area');
		$operatingUnits=DB::table($reference_schema.'.HIERARCHY')->distinct()->where('Active','=',1)->pluck('Operating_Unit');

		foreach ($hierarchy as $row) {
			if(!isset($map[$row->Location_Name])){
				// array_push($map, $row->Location_Name, $obj);
				$map[$row->Location_Name]=$obj;
			}
			// array_push($map[$row->Location_Name], $row->Business_Unit);
			if(!isset($map[$row->Location_Name][$row->Business_Unit])){
				$map[$row->Location_Name][$row->Business_Unit]=$obj;
			}
			if(!isset($map[$row->Location_Name][$row->Business_Unit][$row->Service_Area])){
				$map[$row->Location_Name][$row->Business_Unit][$row->Service_Area]=$obj;
			}
			array_push($map[$row->Location_Name][$row->Business_Unit][$row->Service_Area], $row->Operating_Unit);
		}

		$dropdownData = array('map'=>$map,'locations'=>$locations,'businessUnits'=>$businessUnits,'serviceAreas'=>$serviceAreas,'operatingUnits'=>$operatingUnits);

		return $dropdownData;
	}
				
}


