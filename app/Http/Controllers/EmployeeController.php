<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Config;

class EmployeeController extends BaseController	
{

    public function generateEmployees($cities,$businessUnits,$serviceAreas,$operatingUnits) {

		$citiesArray = explode(',',$cities); 
		$businessUnitsArray = explode(',',$businessUnits);
		$serviceAreasArray = explode(',',$serviceAreas); 
		$operatingUnitsArray = explode(',',$operatingUnits); 

		$schema=config::get('database.connections.sqlsrv.schema');

		$employees = DB::table($schema.'.WWITZ_V2')
										->select($schema.'.WWITZ_V2.2_Service_Area_Style as Service_Area_Style',
											'Employee_Id',
											'Full_Name',
											'Position_List_Order',
											'Position_Name',
											'Location',
											'Qualification',
											'Bio',
											'Mobile_Num',
											'username',
											'1_Business_Unit as Business_Unit',
											'1_Business_Unit_Style as Business_Unit_Style',
											'2_Service_Area as Service_Area',
											'2_Service_Area_Style as Service_Area_Style',
											'3_Operating_Unit as Operating_Unit',
											'3_Operating_Unit_Style as Operating_Unit_Style',
											'FLAG')
										->when($citiesArray!=['all'], function($query) use ($citiesArray) {
									        $query->whereIn('Location',$citiesArray);
									      })
										->when($businessUnitsArray!=['all'], function($query) use ($businessUnitsArray) {
									        $query->whereIn('1_Business_Unit',$businessUnitsArray);
									      })
										->when($serviceAreasArray!=['all'], function($query) use ($serviceAreasArray) {
									        $query->whereIn('2_Service_Area',$serviceAreasArray);
									      })
										->when($operatingUnitsArray!=['all'], function($query) use ($operatingUnitsArray) {
									        $query->whereIn('3_Operating_Unit',$operatingUnitsArray);
									      })
										->orderBy('Position_List_Order','asc')
										->orderBy('Full_Name','asc')
										->get();


		$employeeCount = DB::table($schema.'.WWITZ_V2')
										->when($citiesArray!=['all'], function($query) use ($citiesArray) {
									        $query->whereIn('Location',$citiesArray);
									      })
										->when($businessUnitsArray!=['all'], function($query) use ($businessUnitsArray) {
									        $query->whereIn('1_Business_Unit',$businessUnitsArray);
									      })
										->when($serviceAreasArray!=['all'], function($query) use ($serviceAreasArray) {
									        $query->whereIn('2_Service_Area',$serviceAreasArray);
									      })
										->when($operatingUnitsArray!=['all'], function($query) use ($operatingUnitsArray) {
									        $query->whereIn('3_Operating_Unit',$operatingUnitsArray);
									      })
										->orderBy('Position_List_Order','asc')
										->orderBy('Full_Name','asc')
										->count();

		$employeeData = array('employees'=>$employees,'employeeCount'=>$employeeCount);

		return $employeeData;

	}
				
}


