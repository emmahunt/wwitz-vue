/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

import Vue from 'vue';
import axios from "axios";
import MultiSelect from "vue-multiselect";
import Loading from "vue-loading-overlay";
import 'vue-loading-overlay/dist/vue-loading.css';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// Promises / polyfills to support site in internet explorer
import Es6Promise from 'es6-promise'
Es6Promise.polyfill()
import 'babel-polyfill'

// Import components
import Employee from './components/Employee';
import DropdownMenu from './components/DropdownMenu';
import Role from './components/Role';
import Team from './components/Team';
import NoResults from './components/NoResults';

const initRoute = { template: '<div></div>' }

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/init', component: initRoute }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
});


// Now the app has started!

const employee = new Vue({
    router, 
    components: { Employee, DropdownMenu, Role, NoResults },

    template:`
    <div class="container-fluid">
        <router-view></router-view>
        <div class="row">
    		    <DropdownMenu  v-if="Object.keys(map).length>0" :map="map" :cities="cities" :isLoading="isLoading" :businessUnits="businessUnits" :operatingUnits="operatingUnits" :serviceAreas="serviceAreas" :resultCount="resultCount" @generateBoard="generateBoard">
            </DropdownMenu>
            <div class="col-8">
              <NoResults v-if="resultCount==0">
              </NoResults>
              <div v-for="role in Object.keys(employeeGroups)" class="row justify-content-left" :id="role">
          			<div class ="row"><h2>{{role}}</h2></div>
                <div class="anchor-row":id="'a '+role">
              </div>
              <Employee v-for="item in employeeGroups[role]" :item="item" :key="item.username">
              </Employee>
              </div>
              </div>
              <role :employeeGroups="employeeGroups">
              </role>
            </div>
        </div>
    </div>
    `,
    el: '#app',
    data: {
    		item:[],
    		message:'',
    		employeeGroups: {},
            dropdownData:{},
    		cities:[],
    		businessUnits:[],
    		serviceAreas:[],
    		operatingUnits:[],
            map:{},
            resultCount:-1,
            isLoading:false,
            team:{'ehunt':['Senior Consultant','Analytics & AI','Front End Developer'],'momansuri':['Graduate','Analytics & AI','Database Developer'],'kburbridge':['Manager','Risk Advisory National',''],'ebirgisson':['Analyst','CON Tech Strategy & Transformation','Front End Developer']}

    },

    mounted: async function(){
        this.loadData();
    },
    methods: {
        loadData: async function(){
            var dropdownData = (await axios.get('./api/mapping'));
            this.map = dropdownData.data["map"];
            this.cities = dropdownData.data["locations"];
            this.businessUnits = dropdownData.data["businessUnits"];
            this.serviceAreas = dropdownData.data["serviceAreas"];
            this.operatingUnits = dropdownData.data["operatingUnits"];
        }

    	,generateBoard: async function(filterCities, filterBusinessUnits,filterServiceAreas,filterOperatingUnits){
        this.isLoading=true;
        var response = await axios.get('./api/employees/'+filterCities+'/'+filterBusinessUnits+'/'+filterServiceAreas+'/'+filterOperatingUnits);
        var employees=response.data["employees"];
        this.resultCount=response.data["employeeCount"];

        this.employeeGroups=[];

      	employees.forEach(employee => {
      		if(!this.employeeGroups[employee.Position_Name]) 
      			Vue.set(this.employeeGroups, employee.Position_Name, []);

      		this.employeeGroups[employee.Position_Name].push(employee);
      	});

        this.isLoading=false;

    	}
    }
});


const team = new Vue({
    components:{Team}
    ,el:'#teamapp'
    ,template:`
    <div class="container-fluid">
        <div class="row" >
        <div class="col-3">
        </div>
        <div class="col-8">
        <div class ="row"><h1>Current WWITZ Team</h1></div>
        <div class ="row"><p>The WWITZ Team maintain and update the digital photoboard site.We welcome any feedback you may have to improve the site. Reach out to us at <a href="mailto:WWitZAustralia@deloitte.com.au">the WWitZ Committee mailing list</a></p></div>
        <Team v-for="teamMember in team" :teamMember="teamMember" :key="teamMember.Full_Name"></Team>
        <div class ="row"><h1>Previous Team Members</h1></div>
        <Team v-for="teamMember in pastTeamMembers" :teamMember="teamMember" :key="teamMember.Full_Name"></Team>
        </div>
        <div class="col">
        </div>
        </div>
    </div>
    `
    ,data: {
      "team":[ 
        { 
        "Service_Area_Style":"Style2",
        "Full_Name":"Emma Hunt",
        "Location":"Perth",
        "username":"ehunt",
        "Operating_Unit":"CON Analytics & AI",
        "description":"Front End Developer"
        },
        { 
        "Service_Area_Style":"Style2",
        "Full_Name":"Zayd Mansuri",
        "username":"momansuri",
        "Location":"Sydney",
        "Operating_Unit":"CON Analytics & AI",
        "description":"Database Developer"
        },
        { 
        "Service_Area_Style":"Style3",
        "Full_Name":"Kellie Burbridge",
        "Position_Name":"Senior Manager",
        "username":"kburbridge",
        "Location":"Melbourne",
        "Operating_Unit":"RA National",
        "description":"Business Unit Coordinator"
        },
        { 
        "Service_Area_Style":"Style12",
        "Full_Name":"Gudlaugur Ellert Birgisson",
        "username":"ebirgisson",
        "Location":"Brisbane",
        "Operating_Unit":"CON Tech Strategy & Transformation",
        "description":"Front End Developer"
        }
      ]
      ,"pastTeamMembers":[{"Full_Name":"Alice McCullagh","username":"amccullagh","description":"Built Original Site in 2016",
                "Location":"Perth",
                "Operating_Unit":"",
                "Operating_Unit_Style":"Style15",
                "FLAG":""},{"Full_Name":"Cam Wasilewsky","username":"cwasilewsky","description":"Site automation, consulting rollout","Location":"Sydney",
                "Operating_Unit":"",
                "Operating_Unit_Style":"Style15",
                "FLAG":""}]


    },
})