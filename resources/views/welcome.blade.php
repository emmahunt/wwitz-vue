<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Who's Who in the Zoo</title>
        <link rel="shortcut icon" href="img/faveicon.ico">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <link rel=stylesheet href="./css/app.css">
        <!-- Matomo -->
        <script type="text/javascript">
             var _paq = window._paq || [];
             /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
             _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
             _paq.push(['trackPageView']);
             _paq.push(['enableLinkTracking']);
             (function() {
               var u="//sandyedge.au.deloitte.com/piwik/";
               _paq.push(['setTrackerUrl', u+'matomo.php']);
               _paq.push(['setSiteId', '10']);
               var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
               g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
             })();
          </script>

      <!-- End Matomo Code -->
   </head>
   <body>
      <div class = "wrapper">
          <nav class="navbar sticky-top navbar-expand-sm navbar-top">
             <div class="title">
                <h1>Who's Who in the Zoo</h1>
                <h2> Deloitte Australia</h2>
             </div>
             <div class="team-container">
            <a class="nav-team" href="team">WWITZ team</a>
          </div>
          </nav>
         
          <div id="app">
             <Employee></Employee>
          </div>
          <div class = "centre main fade-in one" id="photoboard">
          </div>
          <noscript>Your browser either does not support JavaScript, or has it turned off.</noscript>
          <div class="footer">
             <p> V01 Built by Alice McCullagh in 2016 <br />
                Built by the <a href="team">WWITZ Committee</a> : Emma Hunt and Zayd Mansuri <br /> 
                Original Concept from Consulting Chart by Geoff Chong, Angel Chuang, Chris James and Alyssa-Maree O’Brien<br />
             </p>
             {{-- <script src="{{mix('js/app.js')}}"></script> --}}
             <script src="./js/app.js"></script>
             <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
          </div>
        </div>
   </body>
</html>