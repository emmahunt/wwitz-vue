<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{loc}/{sl}', function (location, serviceLine) {
//     return view('allThePeople');
// });


// sandyedge/wwitz?city=blah&sl=consulting

// sandyedge/wwitz/perth/consulting

Route::get('/', function () {
    return view('welcome');
});

Route::get('/team', function () {
    return view('team');
});

// redirect old sites to new site
Route::get('/Adelaide', function () {
    return redirect('http://sandyedge/wwitz/');
});

Route::get('/Brisbane', function () {
    return redirect('http://sandyedge/wwitz/');
});

Route::get('/Canberra', function () {
    return redirect('http://sandyedge/wwitz/');
});

Route::get('/Melbourne', function () {
    return redirect('http://sandyedge/wwitz/');
});

Route::get('/Perth', function () {
    return redirect('http://sandyedge/wwitz/');
});

Route::get('/Sydney', function () {
    return redirect('http://sandyedge/wwitz/');
});

Route::get('/wwitz', 'DropdownController@index');

Route::post('/employees', 'CityDropdown@filter');


